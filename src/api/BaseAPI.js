import axios from "axios";

const api = axios.create({
    baseURL: 'http://localhost:1945/api'
});

export default api;
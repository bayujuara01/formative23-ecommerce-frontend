import { Col, Card, ListGroup, Button } from "react-bootstrap";
import { useNavigate, } from "react-router";
import userImg from "../../assets/images/user.png";

const CardUser = ({ name , userId}) => {
    let navigate = useNavigate();
    const registerStoreLinkHandler = () => {
        navigate({
            pathname: "/register/store/"+userId,
            
        });
    };
    return (
        <Col sm={4}>
            <Card>
                <Card.Img
                    className="bg-light p-4"
                    variant="top"
                    src={userImg}
                />
                <ListGroup className="list-group-flush">
                    <ListGroup.Item
                        as="li"
                        className="d-flex justify-content-between align-items-start"
                    >
                        <div className="ms-2 me-auto">
                            <div className="fw-bold">Username</div>
                            {name}
                        </div>
                    </ListGroup.Item>
                    <Button
                        onClick={registerStoreLinkHandler}
                        variant="outline-success"
                        className="fw-bold m-4"
                    >
                        Store Activate
                    </Button>
                </ListGroup>
            </Card>
        </Col>
    );
};

export default CardUser;

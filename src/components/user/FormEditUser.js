import { Col, Card, Button, Form } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import { useState,useEffect } from "react";
import api from "../../api/BaseAPI";

const FormEditUser = () => {
    let navigate = useNavigate();
    const location = useLocation();
    const [, ,userId,,detailId] = location.pathname.split("/");

    const [detail, setDetail] = useState({
        id: 0,
        address: "",
        telepon: "",
        fullname: ""
    });
    useEffect(() => {
        api.get(`/user/${userId}/detail-user/${detailId}`)
            .then((response) => {
                const data = response.data.data[0];
                setDetail(data);
            })
            .catch((err) => {
                alert("Error fetching data");
            });
    }, [setDetail]);
    console.log(detail.id);

    function fullnameHandler(events) {
        setDetail((prevsValue) => {
            return {
                ...prevsValue,
                fullname: events.target.value,
            };
        });
    }

    function addressHandler(events) {
        setDetail((prevsValue) => {
            return {
                ...prevsValue,
                address: events.target.value,
            };
        });
    }
    function teleponHandler(events) {
        setDetail((prevsValue) => {
            return {
                ...prevsValue,
                telepon: events.target.value,
            };
        });
    }
    
    const handleSubmit = (event) => {
        event.preventDefault();
        api.put("/detail-user/"+detailId, detail)
            .then(response => {
                navigate("/user/"+userId+"/edit-detail/"+detail.id);
                alert("Edit Successfully");
            })
            .catch(err => {
                alert("Edit Gagal");
            })
    }
   
    return (
        <Col sm={12}  className="pt-5">
            <Card className="bg-light">
                <Card.Header>
                    <h5 className="">Detail User</h5>
                </Card.Header>

                <Card.Body>
                    <Form className="pt-3">
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Lengkap</Form.Label>
                            <Form.Control
                                type="text"
                                className="bg-white"
                                value={detail.fullname}
                                onChange={fullnameHandler}
                                placeholder="Masukan Nama Lengkap"
                                
                            />
                          
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                        >
                            <Form.Label>Alamat</Form.Label>
                            <Form.Control
                                as="textarea"
                                type="text"
                                className="bg-white"
                                value={detail.address}
                                onChange={addressHandler}
                                placeholder="Masukan Alamat"
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formBasicEmail"
                        >
                            <Form.Label>Phone</Form.Label>
                            <Form.Control
                                type="phone"
                                className="bg-white"
                                value={detail.telepon}
                                onChange={teleponHandler}
                                placeholder="Masukan Alamat"
                            />
                        </Form.Group>
                    </Form>
                    <Button onClick={handleSubmit} variant="success" className="fw-bold m-4 float-end">
                        Edit
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    );
};
export default FormEditUser;

import { Col, Card, Button, Form } from "react-bootstrap";
import { useNavigate } from "react-router";


const DetailUser = ({ fullname, address, telepon,detailUserId,userId}) => {
    let navigate = useNavigate();
    const editLinkHandler = () => {
        navigate({
            pathname: "/user/"+userId+"/edit-detail/"+detailUserId,
        });
    };

    return (
        <Col sm={8}>
            <Card className="bg-light">
                <Card.Header>
                    <h5 className="">Detail User</h5>
                </Card.Header>

                <Card.Body>
                    <Form className="pt-3">
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Lengkap</Form.Label>
                            <Form.Control
                                type="text"
                                className="bg-white"
                                placeholder="Enter Fullname"
                                value={fullname}
                                disabled
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formBasicEmail"
                        >
                            <Form.Label>Alamat</Form.Label>
                            <Form.Control
                                as="textarea"
                                type="text"
                                className="bg-white"
                                placeholder="Enter Address"
                                value={address}
                                disabled
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formBasicEmail"
                        >
                            <Form.Label>Nomor Telepon</Form.Label>
                            <Form.Control
                                type="phone"
                                className="bg-white"
                                placeholder="Enter Fullname"
                                value={telepon}
                                disabled
                            />
                        </Form.Group>
                    </Form>
                    <Button onClick={editLinkHandler} variant="success" className="fw-bold m-4 float-end">
                        Edit
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    );
};
export default DetailUser;

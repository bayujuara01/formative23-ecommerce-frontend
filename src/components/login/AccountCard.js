import { useEffect, useState } from "react";
import { Card, Container, Button, Row, Col, Form } from "react-bootstrap";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import api from "../../api/BaseAPI";

const AccountCard = () => {

    const navigate = useNavigate();
    const [user, setUser] = useState({ username: '', password: '' });

    const handleChange = (event) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    useEffect(() => {
        getLoginFromLocal();
    }, [])

    const getLoginFromLocal = () => {
        const loginInfo = localStorage.getItem("login");

        if (loginInfo) {
            navigate("/");
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        api.post("/auth/login", user)
            .then(response => {
                alert("Login Berhasil");
                localStorage.setItem("login", JSON.stringify(response.data.data));
                navigate("/");
            })
            .catch(err => {
                alert("Login Gagal");
            })
    }

    return (
        <Card className="mx-auto user-card" style={{ width: '24rem', borderRadius: '8px' }}>

            <Card.Body>
                <Card.Title className="mt-4 mb-4">Masuk</Card.Title>
                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="text-muted user-input__label">Alamat Email</Form.Label>
                        <Form.Control
                            name="username"
                            onChange={handleChange}
                            type="email"
                            placeholder="Masukkan email" />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="text-muted user-input__label">Kata Sandi</Form.Label>
                        <Form.Control
                            name="password"
                            onChange={handleChange}
                            type="password"
                            placeholder="Masukkan Password" />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicCheckbox" >
                        <p className="user-link__right user-input__label">
                            <a href="#home" className="text-success user-link " >Lupa Kata Sandi?</a>
                        </p>
                    </Form.Group>

                </Form>
                <Row className="mb-4">
                    <Col>
                        <Button
                            onClick={handleSubmit}
                            style={{ width: '100%', borderRadius: '8px' }}
                            variant="success" >Masuk</Button>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col className="d-flex justify-content-center align-items-center">
                        <span className="user-account-line"></span>
                        <span className="user-account-line__label text-muted mx-3">Tidak ada akun?</span>
                        <span className="user-account-line"></span>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col>
                        <Link to="/register">
                            <Button style={{ width: '100%', borderRadius: '8px' }} variant="outline-secondary">Daftar</Button>
                        </Link>
                    </Col>
                </Row>
            </Card.Body>

        </Card>
    );
}

export default AccountCard;
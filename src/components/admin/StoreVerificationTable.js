import { Table, ButtonGroup, DropdownButton, Dropdown, Button } from 'react-bootstrap';

export default function StoreVerificationTable() {
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Store Name</th>
                    <th>Username</th>
                    <th>City</th>
                    <th>Store Approval</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Tokoku</td>
                    <td>bayujuara01</td>
                    <td>Purworejo</td>
                    <td>
                        <DropdownButton title="Dropdown" id="bg-nested-dropdown">
                            <Dropdown.Item eventKey="1">Approve</Dropdown.Item>
                            <Dropdown.Item eventKey="2">Cancel</Dropdown.Item>
                        </DropdownButton>
                    </td>
                </tr>
            </tbody>
        </Table>
    );
}
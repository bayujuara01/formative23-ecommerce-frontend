const NotFound = () => {
    return (
        <>
            <p className="text-success fw-bold fs-150">404</p>
            <p className="text-success fw-bold">oopss..</p>
            <p className="text-success fw-bold">Page Not Found</p>
        </>
    );
};
export default NotFound;

import { useContext, useEffect } from 'react';
import { Nav, Navbar, Container, Form, FormControl, Button, Dropdown, DropdownButton, Row, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/tokopaedi-logo.png';
import { NavigationContext } from '../contexts/NavigationContext';
import { SERVER_URL } from '../../constant';

const NavigationBar = ({ login }) => {

    const [nav, setNav] = useContext(NavigationContext);
    const navigate = useNavigate();

    useEffect(() => {
        checkLocalCart();
    }, [])

    const checkLocalCart = () => {
        const cartExist = localStorage.getItem("cart");
        if (cartExist) {
            setNav({
                ...nav,
                cart: JSON.parse(cartExist)
            });
        } else {
            localStorage.setItem("cart", JSON.stringify([]));
        }
    }

    const clickHandler = () => {
        if (nav.login.logged) {
            localStorage.removeItem("login");
            localStorage.removeItem("cart");
            setNav({
                ...nav,
                login: {
                    logged: false
                },
                cart: []
            })
            navigate("/");
        } else {
            navigate("/login");
        }

    }

    useEffect(() => {
        fetchLocalLogin();
    }, []);

    const fetchLocalLogin = () => {
        const local = localStorage.getItem("login");
        if (local) {
            const localLogin = JSON.parse(local);
            setNav({
                ...nav,
                login: {
                    logged: true,
                    ...localLogin
                }
            });
        }
    }

    return (
        <Navbar fixed="top" sticky="top" bg="light" expand="lg"  >
            <Container>
                <Link to="/">
                    <Navbar.Brand>
                        <img
                            src={logo}
                            height="40"
                            className="d-inline-block align-top"
                            alt="React Bootstrap logo"
                        />
                    </Navbar.Brand>
                </Link>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className=" my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                    >
                        {/* <Nav.Link href="#action2">Kategori</Nav.Link> */}
                    </Nav>
                    <Form className="d-flex w-100" >
                        <FormControl
                            type="search"
                            placeholder="Cari Barang"
                            className="me-2"
                            aria-label="Cari Barang"
                            style={{ width: '100%' }}
                        />
                        <Button variant="outline-success" className="mx-2">Cari</Button>
                        <DropdownButton variant="outline-success" className="mx-2" title={`Keranjang [${nav.cart.length}]`}>
                            {nav.cart.map((product) =>
                                <Dropdown.Item key={product.id} style={{ minWidth: '300px' }}>
                                    <Row>
                                        <Col md={3} className="d-flex align-items-center">
                                            <img
                                                src={`${SERVER_URL}/${product.imageUrl}`}
                                                height="60"
                                                alt={`${product.productName}`}
                                            />
                                        </Col>
                                        <Col>
                                            <h5>{product.productName}</h5>
                                            Rp {product.price} <br />
                                            Qty : {product.qty}
                                        </Col>
                                    </Row>
                                </Dropdown.Item>
                            )}
                            <Dropdown.Divider />
                            <Dropdown.Item as={Link} to="/user/cart" >Lihat Keranjang</Dropdown.Item>
                        </DropdownButton>
                        {nav.login.logged ? <>
                            <DropdownButton variant="outline-success" className="mx-2" title={nav.login.username}>
                                <Dropdown.Item style={{ minWidth: '200px' }}>Profil</Dropdown.Item>
                                <Dropdown.Item as={Link} to="/user/cart" style={{ minWidth: '200px' }}>Pesanan</Dropdown.Item>
                                {nav.login.roleName === 'STORE' ? <>
                                    <Dropdown.Divider />
                                    <Dropdown.Item as={Link} to={`/store/${nav.login.storeName}`} style={{ minWidth: '200px' }}>Toko</Dropdown.Item>
                                </> : <></>}
                                <Dropdown.Divider />
                                <Dropdown.Item onClick={clickHandler}>Keluar</Dropdown.Item>
                            </DropdownButton>
                        </> : <>
                            <Button onClick={clickHandler} variant="outline-success">Masuk</Button>
                        </>}

                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;
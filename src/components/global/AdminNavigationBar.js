import { useContext } from 'react';
import { Nav, Navbar, Container, Form, FormControl, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router';
import logo from '../../assets/images/tokopaedi-logo.png';

const AdminNavigationBar = ({ login }) => {
    const navigate = useNavigate();

    const clickHandler = () => {
        navigate("/login");
    }

    return (
        <Navbar fixed="top" sticky="top" bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        src={logo}

                        height="40"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Form className="" >
                        <Button onClick={clickHandler} variant="outline-success">Masuk</Button>
                    </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default AdminNavigationBar;
import { Col, Card, Button, Form } from "react-bootstrap";

const RightProductDetailProduct = ({ name, price, desc, submitHandler }) => {
    return (
        <>
            <Col sm={8} >
                <Card className="bg-light">
                    <Card.Header>
                        <h5>Info Produk</h5>
                    </Card.Header>

                    <Card.Body>
                        <Form className="pt-3">
                            <Form.Group className="mb-3" controlId="formBasicName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    className="bg-white"
                                    value={name}
                                    disabled
                                />
                            </Form.Group>
                            <Form.Group
                                className="mb-3 pt-3"
                                controlId="formBasicDesc"
                            >
                                <Form.Label>Deskripsi Produk</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    type="text"
                                    className="bg-white"
                                    value={desc}
                                    disabled
                                />
                            </Form.Group>
                            <Form.Group
                                className="mb-3 pt-3"
                                controlId="formBasicPrice"
                            >
                                <Form.Label>Harga</Form.Label>
                                <Form.Control
                                    type="phone"
                                    className="bg-white"
                                    value={price}
                                    disabled
                                />
                            </Form.Group>
                        </Form>
                        <Button onClick={submitHandler} variant="success" className="fw-bold m-4 float-end">Tambah Keranjang</Button>
                    </Card.Body>
                </Card>
            </Col>
        </>
    );
}

export default RightProductDetailProduct;
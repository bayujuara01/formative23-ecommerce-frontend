import {
    Card,
    Col
} from "react-bootstrap";

const ProductCard3 = () => {
    return (
        <Col lg={3}>
            <Card onClick="">
                <Card.Img variant="top" src="https://robohash.org/test" />
                <Card.Body>
                    <Card.Text className="fs-086">producname</Card.Text>
                    <Card.Text className="fs-14 fw-bold">RP 100,00</Card.Text>
                    <Card.Text className="fs-086">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ProductCard3;

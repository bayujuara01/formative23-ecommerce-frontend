import { useContext, useEffect, useState } from "react";
import { Col, Card, Button, Form, FloatingLabel } from "react-bootstrap";
import api from "../../api/BaseAPI";
import { NavigationContext } from "../contexts/NavigationContext";

const SaveProduct = () => {

    const [nav] = useContext(NavigationContext);
    const [product, setProduct] = useState({
        name: "",
        price: 0,
        desc: "",
        file: null,
        userId: null
    });

    useEffect(() => {
        setProduct({
            ...product,
            userId: nav.login.id
        })
    }, [])

    const handleChange = (event) => {
        setProduct({
            ...product,
            [event.target.name]: event.target.value
        });
    }

    const handleFileChange = (event) => {
        console.log(event.target.files);
        setProduct({
            ...product,
            file: event.target.files[0]
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        onFormSubmit(product).then(response => {
            console.log(response);
            alert("Tambah Produk Berhasil");
        })
            .catch(err => {
                alert("Tambah Produk tidak berhasil, pastikan Isi semua input");
                console.error(err);
            })

    }

    const onFormSubmit = (prod) => {
        console.log(prod);
        const formData = new FormData();
        formData.append('file', prod.file);
        formData.append('name', prod.name);
        formData.append('desc', prod.desc);
        formData.append('price', prod.price);
        formData.append('user_id', nav.login.id);
        const config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }

        return api.post("/product", formData, config);
    }

    return (
        <Col sm={12} className="pt-5">
            <Card className="bg-light">
                <Card.Header>
                    <h5 className="">Edit Product</h5>
                </Card.Header>

                <Card.Body>
                    <Form className="pt-3" method="post">
                        <Form.Group className="mb-3" controlId="formProductName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                name="name"
                                onChange={handleChange}
                                value={product.name}
                                type="text"
                                className="bg-white"
                                placeholder="Masukan Nama Produk"
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formProductPrice"
                        >
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                name="price"
                                onChange={handleChange}
                                value={product.price}
                                type="number"
                                className="bg-white"
                                placeholder="Masukan Harga Produk"
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formDesc"
                        >
                            <Form.Label>Description</Form.Label>
                            <FloatingLabel controlId="formDesctiption" label="Comments">
                                <Form.Control
                                    name="desc"
                                    onChange={handleChange}
                                    value={product.desc}
                                    as="textarea"
                                    placeholder="Leave a comment here"
                                    style={{ height: '80px' }}
                                />
                            </FloatingLabel>
                        </Form.Group>
                        <Form.Group controlId="formImageFile" className="mb-3">
                            <Form.Label>Default file input example</Form.Label>
                            <Form.Control onChange={handleFileChange} type="file" />
                        </Form.Group>
                    </Form>
                    <Button onClick={handleSubmit} variant="success" className="fw-bold m-4 float-end">
                        Submit
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    );
}

export default SaveProduct;
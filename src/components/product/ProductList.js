import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import api from "../../api/BaseAPI";
import ProductCard from "./ProductCard";


const ProductList = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetchAllProduct();
    }, []);

    const fetchAllProduct = () => {
        api.get("/products")
            .then(response => {
                console.log(response.data.data);
                setProducts([...products, ...response.data.data]);
            })
    }

    return (
        <Container className="my-4">
            <Row className="my-4">
                <h3>New Product</h3>
            </Row>
            <Row className="my-4">
                {products.map((product, index) => (<Col key={index} ><ProductCard id={product.id} name={product.productName} price={product.price} imageUrl={product.imageUrl} /></Col>))}
            </Row>
        </Container>
    );
}

export default ProductList;
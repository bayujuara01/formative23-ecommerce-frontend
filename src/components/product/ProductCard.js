import { Card, Button, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { SERVER_URL } from '../../constant';

const ProductCard = ({ name, price, id, imageUrl }) => {
    return (
        <Card style={{ width: '16rem' }}>
            <Card.Img src={`${SERVER_URL}/${imageUrl}`} />
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text>
                    Toko
                </Card.Text>

            </Card.Body>
            <Card.Footer>
                <Row className="d-flex align-items-center justify-content-between">
                    <Col>
                        <Card.Text>Rp. {price}</Card.Text>
                    </Col>
                    <Col>
                        <Link to={`/products/${id}`}>
                            <Button>Detail</Button>
                        </Link>
                    </Col>
                </Row>


            </Card.Footer>
        </Card>
    );
}

export default ProductCard;
import {
    Card,
    Col
} from "react-bootstrap";

import { SERVER_URL } from "../../constant";

const ProductCardStore = ({ product }) => {

    return (
        <Col lg={4} className="py-2">
            <Card onClick={() => console.log("Clicked")}>
                <Card.Img variant="top" src={`${SERVER_URL}/${product.imageUrl}`} />
                <Card.Body>
                    <Card.Text className="fs-5 fw-bold">{product.productName}</Card.Text>
                    <Card.Text className="fs-14 fw-bold">Rp. {product.price}</Card.Text>
                    <Card.Text className="fs-086">{product.desc.substring(0, 60)}...</Card.Text>
                </Card.Body>
            </Card>
        </Col>
    );
};

export default ProductCardStore;

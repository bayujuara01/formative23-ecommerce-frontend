import { Button, Card, Col, ListGroup } from "react-bootstrap";
import { SERVER_URL } from "../../constant";

const LeftProductDetailCard = ({ name, imageUrl }) => {
    return (
        <Col sm={4}>
            <Card>
                <Card.Header>
                    <h5>{name}</h5>
                </Card.Header>
                <Card.Body className="d-flex justify-content-center" style={{ minHeight: '380px' }}>
                    <Card.Img
                        src={`${SERVER_URL}/${imageUrl}`}
                    />
                </Card.Body>
                <ListGroup.Item
                    as="li"
                    className="d-flex justify-content-between align-items-start"
                >
                    <div className="ms-2 me-auto">
                        <div className="fw-bold">Tokokus</div>
                        Surabaya
                    </div>
                </ListGroup.Item>
            </Card>
        </Col>
    );
}

export default LeftProductDetailCard;
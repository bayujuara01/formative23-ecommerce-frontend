import React, { useState, createContext } from 'react';


const NavigationContext = createContext();

const NavigationProvider = (props) => {
    const [nav, setNav] = useState({
        login: {
            logged: false
        },
        cart: [

        ]
    });
    return (
        <div>
            <NavigationContext.Provider value={[nav, setNav]}>
                {props.children}
            </NavigationContext.Provider>
        </div>
    );
}

export { NavigationContext, NavigationProvider };
import { Col, Card, Button, Form } from "react-bootstrap";

const FormEditStore = () => {
    return (
        <Col sm={12} className="pt-5">
            <Card className="bg-light">
                <Card.Header>
                    <h5 className="">Edit Toko</h5>
                </Card.Header>

                <Card.Body>
                    <Form className="pt-3">
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Nama Toko</Form.Label>
                            <Form.Control
                                type="text"
                                className="bg-white"
                                placeholder="Masukan Nama Toko"
                            />
                        </Form.Group>
                        <Form.Group
                            className="mb-3 pt-3"
                            controlId="formBasicEmail"
                        >
                            <Form.Label>Kota</Form.Label>
                            <Form.Control
                                type="text"
                                className="bg-white"
                                placeholder="Masukan Nama Kota"
                            />
                        </Form.Group>
                    </Form>
                    <Button variant="success" className="fw-bold m-4 float-end">
                        Edit
                    </Button>
                </Card.Body>
            </Card>
        </Col>
    );
};
export default FormEditStore;

import { useContext, useState } from "react";
import { Col, Card, Button, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { NavigationContext } from "../contexts/NavigationContext";
import ProductCardStore from "../product/ProductCardStore";

const ListOfStoreProduct = ({ products }) => {

    const [nav, setNav] = useContext(NavigationContext);
    const [listProduct, setListProduct] = useState([{}]);
    const testArr = [1, 2, 3, 4, 5, 6];

    return (
        <Col sm={8} className="pt-1">
            <Card className="bg-light">
                <Card.Header>
                    <Card.Text className="fw-bold">
                        Daftar Produk
                        {nav.login.roleName === 'STORE' ? <>
                            <Link to="/products/new">
                                <Button variant="success" className=" float-end">Tambah Produk</Button>
                            </Link>
                        </> : <></>}

                    </Card.Text>
                </Card.Header>
                <Card.Body>
                    <Row>
                        {products.map((product) => (<ProductCardStore key={product.id} product={product} />))}
                    </Row>
                </Card.Body>
            </Card>
        </Col>
    );
};
export default ListOfStoreProduct;

import { Card, Button, Row, Col, Form } from "react-bootstrap";
const FormRegisterStore = () => {
    return (
        <Card className="mx-auto user-card border-radius-8 w-50">
            <Card.Body>
                <Card.Title className="mt-4 mb-4">Aktivasi Toko</Card.Title>
                <Form>
                    <Form.Group className="mb-4">
                        <Form.Label className="text-muted user-input__label">
                            Nama Toko
                        </Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Masukkan Nama Toko"
                        />
                    </Form.Group>

                    <Form.Group className="mb-4">
                        <Form.Label className="text-muted user-input__label">
                            Kota
                        </Form.Label>
                        <Form.Control type="text" placeholder="Masukkan Kota" />
                    </Form.Group>
                </Form>
                <Row className="mb-4">
                    <Col>
                        <Button
                            className="w-100 border-radius-8"
                            variant="success"
                        >
                            Aktivasi Sekarang
                        </Button>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    );
};
export default FormRegisterStore;

import { useContext, useEffect } from "react";
import { Col, Card, ListGroup, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { NavigationContext } from "../contexts/NavigationContext";

const DetailStore = ({ store }) => {

    const [nav, setNav] = useContext(NavigationContext);
    let navigate = useNavigate();

    useEffect(() => {

    }, []);

    // console.log(nav);
    const editStoreLinkHandler = (param) => {
        navigate(
            {
                pathname: "/store/edit/:storeId"
            }
        );
    };

    return (
        <Col sm={4}>
            <Card>
                <h2 className="text-center p-3">{store.storeName}</h2>
                <ListGroup className="list-group-flush">
                    <ListGroup.Item
                        as="li"
                        className="bg-light d-flex justify-content-between align-items-start"
                    >
                        <div className="ms-2 me-auto">
                            <div className="fw-bold">Kota</div>
                            <p>{store.city}</p>
                        </div>
                    </ListGroup.Item>
                    {nav.login.logged ? <>
                        <Button variant="outline-success" className="fw-bold m-4" onClick={editStoreLinkHandler}>
                            Edit Toko
                        </Button>
                    </> : <></>}

                </ListGroup>
            </Card>
        </Col>
    );
};

export default DetailStore;

import { Card, Col, Container, ListGroup, Row, Button } from "react-bootstrap";

export default function RightTransactionCard() {
    return (
        <>
            <Card>
                <Card.Header>
                    <h5>Detail Pesanan</h5>
                </Card.Header>
                <Card.Body className="d-flex justify-content-center" style={{ minHeight: '380px' }}>
                    <Container>
                        <Row>
                            <Card>
                                <Card.Body className="d-flex justify-content-between">
                                    <Row className="">
                                        <Col>
                                            <h6>Nama Barang</h6>
                                            <h6>Harga</h6>
                                            <h6>Qty</h6>
                                        </Col>
                                    </Row>
                                    <Row className="">
                                        <Col className="d-flex flex-column justify-content-end">
                                            <h5>Rp. 10.000.000</h5>
                                            <Button>Detail</Button>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Row>
                    </Container>
                </Card.Body>
                <ListGroup.Item
                    as="li"
                    className="d-flex justify-content-center"
                >
                    <div className="d-grid gap-2">
                        <Button variant="success" >Checkout</Button>
                    </div>
                </ListGroup.Item>
            </Card>
        </>
    );
}
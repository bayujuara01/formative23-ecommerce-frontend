import { Card, Col, Container, ListGroup, Row, Button } from "react-bootstrap";
import { currencyRupiah } from '../../util/StringFormat';

export default function CartCard({ cart, handler, login }) {
    return (
        <>
            <Card>
                <Card.Header>
                    <h5>Isi Keranjang</h5>
                </Card.Header>
                <Card.Body className="d-flex justify-content-center" style={{ minHeight: '380px' }}>
                    <Container>
                        {cart.map(product => {
                            return (
                                <Row className="my-2" key={product.id}>
                                    <Card>
                                        <Card.Body className="d-flex justify-content-between">
                                            <Row className="">
                                                <Col>
                                                    <h4>{product.productName}</h4>
                                                    <h6>{currencyRupiah(product.price)}</h6>
                                                    <h6>Qty : {product.qty}</h6>
                                                </Col>
                                            </Row>
                                            <Row className="">
                                                <Col className="d-flex flex-column justify-content-end">
                                                    <h5>{currencyRupiah(product.price * product.qty)}</h5>
                                                    <Button>-</Button>
                                                </Col>
                                            </Row>
                                        </Card.Body>
                                    </Card>
                                </Row>
                            )
                        })}
                    </Container>
                </Card.Body>
                <ListGroup.Item
                    as="li"
                    className="d-flex justify-content-center"
                >
                    {login.logged ? <>
                        <div className="d-grid gap-2">
                            <Button variant="success" onClick={handler} >Checkout</Button>
                        </div>
                    </> : <></>}

                </ListGroup.Item>
            </Card>
        </>
    );
}
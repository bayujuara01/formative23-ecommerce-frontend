import currency from 'currency.js';

export const currencyRupiah = (value = 0) => currency(value, { symbol: "Rp. ", separator: ".", precision: 0 }).format();
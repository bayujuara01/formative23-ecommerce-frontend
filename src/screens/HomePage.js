import NavigationBar from '../components/global/NavigationBar.js';
import api from '../api/BaseAPI.js';
import ProductList from '../components/product/ProductList.js';
import HeroCarousel from '../components/home/HeroCarousel.js';

const HomePage = () => {

    return (
        <>
            <NavigationBar />
            <HeroCarousel />
            <ProductList />
        </>
    );
}

export default HomePage;
import NavigationBar from "../components/global/NavigationBar.js";
import NotFound from "../components/global/NotFound.js";
import { Container } from "react-bootstrap";

const NotFoundPage = () => {
    return (
        <>
            <NavigationBar />
            <Container className="pt-3 text-center">
                <NotFound/>
            </Container>
        </>
    );
};

export default NotFoundPage;

import '../assets/css/Login.css';
import { Container } from 'react-bootstrap';
import AccountCard from '../components/login/AccountCard';

const LoginPage = () => {
    return (
        <Container>
            <AccountCard />
        </Container>
    );
}

export default LoginPage;
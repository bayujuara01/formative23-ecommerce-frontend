import { useContext, useEffect, useState } from "react";
import { Card, Col, Container, ListGroup, Row, Button } from "react-bootstrap";
import api from "../api/BaseAPI";
import { NavigationContext } from "../components/contexts/NavigationContext";
import NavigationBar from "../components/global/NavigationBar";
import CartCard from "../components/transactions/CartCard";
import RightTransactionCard from "../components/transactions/RightTransactionCard";
import { currencyRupiah } from "../util/StringFormat";

export default function ListTransactionPage() {

    const [nav, setNav] = useContext(NavigationContext);
    const [cart, setCart] = useState({
        isCart: true,
        grandPrice: 0
    });


    useEffect(() => {
        checkLocalCart();
        calculateTotalCart();
    }, [])

    const calculateTotalCart = () => {
        let price = 0
        for (let product of nav.cart) {
            price += (product.price * product.qty);
        }
        setCart({
            ...cart,
            grandPrice: price
        })
    }

    const checkLocalCart = () => {
        const cartExist = localStorage.getItem("cart");
        if (cartExist) {
            setNav({
                ...nav,
                cart: JSON.parse(cartExist)
            });
        } else {
            localStorage.setItem("cart", JSON.stringify([]));
        }
    }

    const submitCheckoutHandler = () => {
        const productSubmit = nav.cart.map((prod) => {
            return {
                product: {
                    id: prod.id
                },
                qty: prod.qty
            }
        });

        api.post(`/transaction/create/${nav.login.id}`, productSubmit)
            .then(response => {
                console.log(response);
                setNav({
                    ...nav,
                    cart: []
                });
                setCart({
                    ...cart,
                    grandPrice: 0
                });
                localStorage.removeItem("cart");
                alert("Checkout Berhasil");
            }).catch(err => {
                console.error(err);
                alert("Checkout Error, Try again");
            });
    }

    const fetchTransaction = () => {
        api.get("/transaction")
    }

    return (
        <>
            <NavigationBar />
            <Container className="mt-4">
                <Row>
                    <Col md={5}>
                        <Card>
                            <Card.Header>
                                <h5>Keranjang</h5>
                            </Card.Header>
                            <Card.Body className="d-flex justify-content-center" style={{ minHeight: '380px' }}>
                                <Container>
                                    <Row>
                                        <Card bg="primary" text="light">
                                            <Card.Body className="d-flex justify-content-between">
                                                <Row className="">
                                                    <Col>
                                                        <h6>Keranjang</h6>
                                                        <h6>Total Barang</h6>
                                                        <h6>Total Harga</h6>
                                                    </Col>
                                                </Row>
                                                <Row className="">
                                                    <Col className="d-flex flex-column justify-content-end">
                                                        <h5>{currencyRupiah(cart.grandPrice)}</h5>
                                                        <Button
                                                            onClick={() => setCart({ ...cart, isCart: true })}
                                                            variant="outline-light" >Detail</Button>
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Card>
                                    </Row>
                                    <Row className="mt-3">
                                        <Card>
                                            <Card.Body className="d-flex justify-content-between">
                                                <Row className="">
                                                    <Col>
                                                        <h6>Pesanan</h6>
                                                        <h6>Status</h6>
                                                    </Col>
                                                </Row>
                                                <Row className="">
                                                    <Col className="d-flex flex-column justify-content-end">
                                                        <h5>Rp. 10.000.000</h5>
                                                        <Button
                                                            variant="primary" >Detail</Button>
                                                    </Col>
                                                </Row>
                                            </Card.Body>
                                        </Card>
                                    </Row>
                                </Container>
                            </Card.Body>
                            <ListGroup.Item
                                as="li"
                                className="d-flex justify-content-between align-items-start"
                            >
                                <div className="ms-2 me-auto">
                                    <div className="fw-bold">Tokokus</div>
                                    Surabaya
                                </div>
                            </ListGroup.Item>
                        </Card>
                    </Col>
                    <Col md={7}>
                        {cart.isCart ? <CartCard cart={nav.cart} login={nav.login} handler={submitCheckoutHandler} /> : <RightTransactionCard />}
                    </Col>
                </Row>
            </Container>
        </>
    );
}
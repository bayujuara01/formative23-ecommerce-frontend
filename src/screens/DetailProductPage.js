import { useContext, useEffect, useState } from "react";
import { Container, Row } from "react-bootstrap";
import { useLocation } from "react-router";
import NavigationBar from "../components/global/NavigationBar";
import LeftProductDetailCard from "../components/product/LeftProductDetailCard";
import RightProductDetailProduct from "../components/product/RightProductDetailProduct";
import api from "../api/BaseAPI";
import { NavigationContext } from "../components/contexts/NavigationContext";

const DetailProductPage = () => {

    const [nav, setNav] = useContext(NavigationContext);
    const location = useLocation();
    const [, , productId] = location.pathname.split("/");
    const [product, setProduct] = useState({
        imageUrl: '',
        productName: '',
        price: '',
        desc: '',
        store: {},
        qty: 0
    });

    useEffect(() => {
        checkLocalCart();
        api.get(`product/${productId}`).then(response => {
            setProduct({
                ...product,
                ...response.data.data[0]
            });

        }).catch(err => {
            alert("Error fetching data");
        });

    }, []);

    const submitProductToCart = (event) => {
        console.log(checkIfProductExist(nav.cart, product.id));
        // return;
        const indexProduct = checkIfProductExist(nav.cart, product.id);
        if (indexProduct >= 0) {
            console.log("OK");
            const productCart = [...nav.cart];
            const selectedProduct = productCart[indexProduct];
            selectedProduct.qty = selectedProduct.qty + 1;
            productCart[indexProduct] = selectedProduct;
            setNav({
                ...nav,
                cart: productCart

            });
        } else {
            setNav({
                ...nav,
                ...nav.cart.push({
                    ...product,
                    qty: 1
                })
            });
        }



        syncLocalCart(nav.cart);
    }

    const checkIfProductExist = (cart, reqId) => {

        return cart.findIndex(product => product.id === reqId);
    }

    const checkLocalCart = () => {
        const cartExist = localStorage.getItem("cart");
        if (cartExist) {
            setNav({
                ...nav,
                cart: JSON.parse(cartExist)
            });
        } else {
            localStorage.setItem("cart", JSON.stringify([]));
        }
    }

    const syncLocalCart = (c) => {
        localStorage.setItem("cart", JSON.stringify(c));
    }

    return (
        <>
            <NavigationBar />
            <Container className="pt-3">
                <Row>
                    <LeftProductDetailCard name={product.productName} imageUrl={product.imageUrl} />
                    <RightProductDetailProduct name={product.productName} price={product.price} desc={product.desc} submitHandler={submitProductToCart} />
                </Row>
            </Container>
        </>

    );

}

export default DetailProductPage;
import { useState } from "react";
import { Card, Button, Row, Col, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import api from '../api/BaseAPI';

const RegisterPage = () => {

    const navigate = useNavigate();
    const [user, setUser] = useState({ username: '', password: '' });

    const handleChange = (event) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        api.post("/users", user)
            .then(response => {
                navigate("/");
            })
            .catch(err => {
                alert("Register Gagal");
            })
    }

    return (
        <Card className="mx-auto user-card" style={{ width: '24rem', borderRadius: '8px' }}>

            <Card.Body>
                <Card.Title className="mt-4 mb-4">Register</Card.Title>
                <Form>
                    <Form.Group className="mb-4" controlId="formBasicEmail">
                        <Form.Label className="text-muted user-input__label">Alamat Email</Form.Label>
                        <Form.Control
                            name="username"
                            value={user.username}
                            onChange={handleChange}
                            type="email"
                            placeholder="Masukkan email" />
                    </Form.Group>

                    <Form.Group className="mb-4" controlId="formBasicPassword">
                        <Form.Label className="text-muted user-input__label">Kata Sandi</Form.Label>
                        <Form.Control
                            name="password"
                            value={user.password}
                            type="password"
                            onChange={handleChange}
                            placeholder="Masukkan Password" />
                    </Form.Group>

                </Form>
                <Row className="mb-4">
                    <Col>
                        <Button
                            style={{ width: '100%', borderRadius: '8px' }}
                            variant="success"
                            onClick={handleSubmit}>Daftar</Button>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col className="d-flex justify-content-center align-items-center">
                        <span className="user-account-line"></span>
                        <span className="user-account-line__label text-muted mx-3">Sudah punya akun?</span>
                        <span className="user-account-line"></span>
                    </Col>
                </Row>
                <Row className="mb-4">
                    <Col>
                        <Link to="/login">
                            <Button style={{ width: '100%', borderRadius: '8px' }} variant="outline-secondary">Login</Button>
                        </Link>
                    </Col>
                </Row>
            </Card.Body>
        </Card >
    );
}

export default RegisterPage;
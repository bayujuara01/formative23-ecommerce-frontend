import { useContext, useEffect } from "react";
import { Container } from "react-bootstrap";
import { NavigationContext } from "../components/contexts/NavigationContext";
import NavigationBar from '../components/global/NavigationBar';
import SaveProduct from "../components/product/SaveProduct";

export default function EditorProductPage() {

    const [nav, setNav] = useContext(NavigationContext);

    useEffect(() => {
        fetchLocalLogin();
    }, []);

    const fetchLocalLogin = () => {
        const local = localStorage.getItem("login");
        if (local) {
            const localLogin = JSON.parse(local);
            setNav({
                ...nav,
                login: {
                    logged: true,
                    ...localLogin
                }
            });
        }
    }

    return (
        <>
            <NavigationBar />
            <Container>
                <SaveProduct />
            </Container>
        </>
    );
}
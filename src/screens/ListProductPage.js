import NavigationBar from '../components/global/NavigationBar.js';
import api from '../api/BaseAPI.js';
import ProductList2 from '../components/product/ProductList2.js';

const ListProductPage = () => {

    return (
        <>
            <NavigationBar />
            <ProductList2 />
        </>
    );
}

export default ListProductPage;
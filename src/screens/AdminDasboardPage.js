import { Container, Row, Col } from "react-bootstrap";
import StoreVerificationTable from "../components/admin/StoreVerificationTable";
import AdminNavigationBar from "../components/global/AdminNavigationBar";

const AdminDashboardPage = () => {
    return (
        <>
            <AdminNavigationBar />
            <Container className="pt-3">
                <Row>
                    <Col>
                        <StoreVerificationTable />
                    </Col>
                </Row>
            </Container>
        </>
    );
}

export default AdminDashboardPage;
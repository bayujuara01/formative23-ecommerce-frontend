import NavigationBar from "../components/global/NavigationBar";
import FormEditUser from "../components/user/FormEditUser";
import { Container } from "react-bootstrap";
const EditUserPage = () => {
    return (
        <>
            <NavigationBar />
            <Container>
                <FormEditUser />
            </Container>
        </>
    );  
};
export default EditUserPage;

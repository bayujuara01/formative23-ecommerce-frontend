import NavigationBar from "../components/global/NavigationBar.js";
import CardUser from "../components/user/CardUser";
import DetailUser from "../components/user/DetailUser";
import { useLocation } from "react-router";
import { useState, useEffect } from "react";
import api from "../api/BaseAPI";
import { Container, Row } from "react-bootstrap";

const DetailUserPage = () => {
    const location = useLocation();
    const [, , userId] = location.pathname.split("/");

    const [user, setUser] = useState({
        id: 0,
        username: "",
        address: "",
        telepon: "",
        fullname: "",
        detailUser: 0,
        role: 0,
    });
    useEffect(() => {
        api.get(`/users/${userId}`)
            .then((response) => {
                const data = response.data.data;
                setUser(data);
            })
            .catch((err) => {
                alert("Error fetching data");
            });
    }, [setUser]);
console.log(user);
    return (
        <>
            <NavigationBar />
            <Container className="pt-3">
                <Row>
                    <CardUser
                        name={user.username}
                        userId={user.id}
                        detailUserId={user.detailUser}
                    />
                    <DetailUser
                        userId={user.id}
                        fullname={user.fullname}
                        address={user.address}
                        telepon={user.telepon}
                        detailUserId={user.detailUser}
                    />
                </Row>
            </Container>
        </>
    );
};

export default DetailUserPage;

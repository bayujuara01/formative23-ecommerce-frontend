import NavigationBar from "../components/global/NavigationBar.js";
import api from "../api/BaseAPI.js";
import FormEditStore from "../components/store/FormEditStore.js";
import { Container } from "react-bootstrap";
const EditStorePage = () => {
    return (
        <>
            <NavigationBar />
            <Container>
                <FormEditStore />
            </Container>
        </>
    );
};

export default EditStorePage;

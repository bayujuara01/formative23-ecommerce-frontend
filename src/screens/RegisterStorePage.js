import NavigationBar from "../components/global/NavigationBar.js";
import FormRegisterStore from "../components/store/FormRegisterStore.js";
import {
    Container,
    Row
} from "react-bootstrap";

const NotFoundPage = () => {
    return (
        <>
            <NavigationBar />
            <Container className="pt-3">
            <FormRegisterStore/>
            </Container>
        </>
    );
};

export default NotFoundPage;

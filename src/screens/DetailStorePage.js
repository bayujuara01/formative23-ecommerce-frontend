import NavigationBar from "../components/global/NavigationBar.js";
import api from "../api/BaseAPI.js";
import ListOfStoreProduct from "../components/store/ListOfStoreProduct.js";
import DetailStore from "../components/store/DetailStore.js";
import {
    Container,
    Row
} from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import { NavigationContext } from '../components/contexts/NavigationContext';
import { useLocation } from "react-router";

const DetailStorePage = () => {

    const [, , storeName] = useLocation().pathname.split("/");
    const [nav, setNav] = useContext(NavigationContext);
    const [store, setStore] = useState({
        products: []
    });

    console.log(storeName);

    useEffect(() => {
        fetchLocalLogin();
        fetchStoreProduct();
    }, []);

    const fetchLocalLogin = () => {
        const local = localStorage.getItem("login");
        if (local) {
            const localLogin = JSON.parse(local);
            setNav({
                ...nav,
                login: {
                    logged: true,
                    ...localLogin
                }
            });
        }
    }

    const fetchStoreProduct = async () => {
        try {
            const response = await api.get(`/store/${storeName}/products`);
            setStore({
                ...store,
                ...response.data.data.store,
                products: [...response.data.data.products]
            });
        } catch (err) {
            console.error(err);
        }
    }

    return (
        <>
            <NavigationBar />
            <Container className="pt-3">
                <Row>
                    <DetailStore store={store} />
                    <ListOfStoreProduct products={store.products} />
                </Row>
            </Container>
        </>
    );
};

export default DetailStorePage;

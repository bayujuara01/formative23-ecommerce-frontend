import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import NotFoundPage from "./screens/NotFoundPage";
import LoginPage from "./screens/LoginPage";
import HomePage from "./screens/HomePage";
import RegisterPage from "./screens/RegisterPage";
import RegisterStorePage from "./screens/RegisterStorePage";
import DetailStorePage from "./screens/DetailStorePage";
import DetailUserPage from "./screens/DetailUserPage";
import EditUserPage from "./screens/EditUserPage";
import EditStorePage from "./screens/EditStorePage";
import ListProductPage from "./screens/ListProductPage";
import { Route, Routes } from "react-router";
import DetailProductPage from "./screens/DetailProductPage";
import AdminDashboardPage from "./screens/AdminDasboardPage";
import EditorProductPage from "./screens/EditProductPage";
import { NavigationProvider } from "./components/contexts/NavigationContext";
import ListTransactionPage from "./screens/ListTransactionPage";

function App() {
    return (
        <NavigationProvider>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/user/1" element={<DetailUserPage />} />
                <Route path="/user/edit" element={<EditUserPage />} />
                {/* User Transaction (Cart) */}
                <Route path="/user/cart" element={<ListTransactionPage />} />
                <Route path="/user/:userId" element={<DetailUserPage />} />
                <Route path="/user/:userId/edit-detail/:detailId" element={<EditUserPage />} />
                {/* Not Found Page */}
                <Route path="*" element={<NotFoundPage />} />
                {/* Store route group */}
                <Route path="/register/store" element={<RegisterStorePage />} />
                <Route path="/store/:storeName" element={<DetailStorePage />} />
                <Route path="/store/edit/:storeId" element={<EditStorePage />} />
                {/* Product */}
                <Route path="/products" element={<ListProductPage />} />
                <Route path="/products/:id" element={<DetailProductPage />} />
                {/* Admin */}
                <Route path="/admin" element={<AdminDashboardPage />} />
                <Route path="/products/new" element={<EditorProductPage />} />
            </Routes>
        </NavigationProvider>
    );
}

export default App;
